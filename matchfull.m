clc; clear; close all;

fname=fullfile('/home/zac/MEGA/McMaster/Thesis/tcpcap/uo2/compfull');
fid=fopen(fname);
compfull=textscan(fid,'%d %f %f %d %d %d %s');
fclose(fid);

fname=fullfile('/home/zac/MEGA/McMaster/Thesis/tcpcap/uo2/rpi1full');
fid=fopen(fname);
rpi1full=textscan(fid,'%d %f %f %d %d %d %s');
fclose(fid);

fname=fullfile('/home/zac/MEGA/McMaster/Thesis/tcpcap/uo2/rpi2full');
fid=fopen(fname);
rpi2full=textscan(fid,'%d %f %f %d %d %d %s');
fclose(fid);

mac='c4:b3:01:c4:b6:e9';
% mac='d8:c7:c8:f2:66:10'

maclist=compfull{7};
[nrow,ncol]=size(maclist);
comp=[];
compext=[];
for i=1:1:6
    compext=[compext compfull{i}];
end

for i=1:1:nrow
    if strcmp(maclist{i},mac)
        comp=[comp; compext(i,:)];
    end
end
maclist=rpi1full{7};
[nrow,ncol]=size(maclist);
rpi1=[];
rpi1ext=[];
for i=1:1:6
    rpi1ext=[rpi1ext rpi1full{i}];
end

for i=1:1:nrow
    if strcmp(maclist{i},mac)
        rpi1=[rpi1; rpi1ext(i,:)];
    end
end
maclist=rpi2full{7};
[nrow,ncol]=size(maclist);
rpi2=[];
rpi2ext=[];
for i=1:1:6
    rpi2ext=[rpi2ext rpi2full{i}];
end

for i=1:1:nrow
    if strcmp(maclist{i},mac)
        rpi2=[rpi2; rpi2ext(i,:)];
    end
end
comp=double(comp);
rpi1=double(rpi1);
rpi2=double(rpi2);

nano = 1E9;
mega = 1E6;
c = 3E8;

comptx = 20; %dbm
compantgain = 5; %dbi
rpi1tx = 30;
rpi1antgain = 6;
rpi2tx = 10;
rpi2antgain = 4;


[rc,cc]=size(comp);
[r1,c1]=size(rpi1);
[r2,c2]=size(rpi2);
k=1;
result=[];
check_table=1:1:4096;
for check = 1:1:4096
    rsc=0;
    rs1=0;
    rs2=0;
    for i=1:1:rc
        if(check == comp(i,5))
           rsc=1;
           break;
        else
           rsc=0;
        end
    end
    
    for i=1:1:r1
        if(check == rpi1(i,5))
           rs1=1;
           break;
        else
           rs1=0;
        end
    end
    
    for i=1:1:r2
        if(check == rpi2(i,5))
           rs2=1;
           break;
        else
           rs2=0;
        end
    end
%     if rsc+rs1+rs2 == 3
        result=[result; rsc rs1 rs2];
%     end
    k=k+1;
end


same_sequence_number=find(sum(result,2)==3);
seq_num = check_table(find(sum(result,2)==3))';


figure(1)
subplot(3,1,1)
plot(comp(:,4)),grid;
title('Comp')

subplot(3,1,2)
plot(rpi1(:,4)),grid;
title('RPi1')
ylabel('Signal Strength (dbm)');

subplot(3,1,3)
plot(rpi2(:,4)),grid;
title('RPi2')
xlabel('Sample');

compstep = rem(comp(:,2),1E3) .* nano + comp(:,3);
rpi1step = rem(rpi1(:,2),1E3) .* nano + rpi1(:,3);
rpi2step = rem(rpi2(:,2),1E3) .* nano + rpi2(:,3);

figure(2)
plot(compstep, comp(:,4)),grid;
hold 'on'
plot(rpi1step, rpi1(:,4));
plot(rpi2step, rpi2(:,4));
legend('Comp','RPi1','RPi2');
xlabel('ns'); ylabel('dbm');

% orderc=1.5;
% wallc=-18;
% comppl = - (comp(:,4) - comptx - compantgain - wallc)./orderc;
% compd = sqrt(10 .^ (comppl ./ 10)) .* c ./ (4 .* pi .* comp(:,6) .* mega);
% order1=2;
% wall1=-12;
% rpi1pl = - (rpi1(:,4) - rpi1tx - rpi1antgain - wall1)./order1;
% rpi1d = sqrt(10 .^ (rpi1pl ./ 10)) .* c ./ (4 .* pi .* rpi1(:,6) .* mega);
% order2=1.5;
% wall2=-4;
% rpi2pl = - (rpi2(:,4) - rpi2tx - rpi2antgain - wall2)./order2;
% rpi2d = sqrt(10 .^ (rpi2pl ./ 10)) .* c ./ (4 .* pi .* rpi2(:,6) .* mega);
orderc=2;
wallc=0;
comppl = - (comp(:,4) - comptx - compantgain - wallc)./orderc;
compd = sqrt(10 .^ (comppl ./ 10)) .* c ./ (4 .* pi .* comp(:,6) .* mega);
order1=2;
wall1=0;
rpi1pl = - (rpi1(:,4) - rpi1tx - rpi1antgain - wall1)./order1;
rpi1d = sqrt(10 .^ (rpi1pl ./ 10)) .* c ./ (4 .* pi .* rpi1(:,6) .* mega);
order2=2;
wall2=0;
rpi2pl = - (rpi2(:,4) - rpi2tx - rpi2antgain - wall2)./order2;
rpi2d = sqrt(10 .^ (rpi2pl ./ 10)) .* c ./ (4 .* pi .* rpi2(:,6) .* mega);



figure(3)
plot(compstep, compd),grid;
hold 'on'
plot(rpi1step, rpi1d);
plot(rpi2step, rpi2d);
legend('Comp','RPi1','RPi2');
xlabel('ns'); ylabel('distance(m)');

comp_ext=[];
for i=1:1:rc
    for j=1:1:length(seq_num)
        if(comp(i,5) == seq_num(j)) 
            comp_ext=[comp_ext; comp(i,:)];
        end
    end
end

rpi1_ext=[];
for i=1:1:r1
    for j=1:1:length(seq_num)
        if(rpi1(i,5) == seq_num(j)) 
            rpi1_ext=[rpi1_ext; rpi1(i,:)];
        end
    end
end

rpi2_ext=[];
for i=1:1:r2
    for j=1:1:length(seq_num)
        if(rpi2(i,5) == seq_num(j)) 
            rpi2_ext=[rpi2_ext; rpi2(i,:)];
        end
    end
end

%extract matched sequence number
%combine time

figure(6)
plot(comp_ext(:,3)),grid;
hold on
plot(rpi1_ext(:,3))
ylabel('Time (ns)')
plot(rpi2_ext(:,3))
legend('comp','rpi1','rpi2')




compstep_ext = rem(comp_ext(:,2),1E3) .* nano + comp_ext(:,3);
rpi1step_ext = rem(rpi1_ext(:,2),1E3) .* nano + rpi1_ext(:,3);
rpi2step_ext = rem(rpi2_ext(:,2),1E3) .* nano + rpi2_ext(:,3);

% compstep_ext-rpi1step_ext
% rpi2step_ext-compstep_ext
comp_ext(:,3)=[];
comp_ext(:,2)=compstep_ext ./ nano;
rpi1_ext(:,3)=[];
rpi1_ext(:,2)=rpi1step_ext ./ nano;
rpi2_ext(:,3)=[];
rpi2_ext(:,2)=rpi2step_ext ./ nano;



%TDOA
Rc1=c .* (comp_ext(:,2)-rpi1_ext(:,2));
Rc2=c .* (comp_ext(:,2)-rpi2_ext(:,2));
R12=c .* (rpi1_ext(:,2)-rpi2_ext(:,2));
R1c=c .* (rpi1_ext(:,2)-comp_ext(:,2));
R2c=c .* (rpi2_ext(:,2)-comp_ext(:,2));
R21=c .* (rpi2_ext(:,2)-rpi1_ext(:,2));

% Rc1=Rc1 ./ mega .* 2;
% R12=R12 ./ mega .* 2;
% R2c=R2c ./ mega .* 2;

% figure(98)
% plot(Rc1),grid;
% hold on
% plot(Rc2)
% plot(R12)
% plot(R1c)
% plot(R21)
% plot(R2c)


figure(5)
subplot(3,1,1)
plot(Rc1),grid;
title('Rc-R1'); ylabel('m')
subplot(3,1,2)
plot(R12),grid;
title('R1-R2'); ylabel('m')
subplot(3,1,3)
plot(R2c),grid;
title('R2-Rc'); ylabel('m')

% R2=(Rc1+R12+R2c)./2;
% Rc=-(R2c-R2);
% R1=-(Rc1-Rc);
% figure(10)
% subplot(3,1,1)
% plot(Rc),grid;
% title('Rc'); ylabel('m')
% subplot(3,1,2)
% plot(R1),grid;
% title('R1'); ylabel('m')
% subplot(3,1,3)
% plot(R2),grid;
% title('R2'); ylabel('m')

% figure(7)
% plot(0,0,'rx');
% hold on
% plot(-5.13,0,'rx');
% plot(0,2.25,'rx');

% for i=1:1:length(R12)
%     ez1=fplot(@(x) (2.25^3 + sqrt(2.25^4.*R12(i)^2 + 2.*2.25^2 -5.13^2.* R12(i)^2 - 4.* 2.25^2 -5.13.* x.* R12(i)^2 + 4.* 2.25^2.* x^2.* R12(i)^2 - 2.* 2.25^2.* R12(i)^4 + -5.13^4.* R12(i)^2 - 4 -5.13^3.* x.* R12(i)^2 + 4 -5.13^2.* x^2.* R12(i)^2 - 2 -5.13^2.* R12(i)^4 + 4 -5.13.* x.* R12(i)^4 - 4.* x^2.* R12(i)^4 + R12(i)^6) - 2.25 -5.13^2 + 2.* 2.25 -5.13 .*x - 2.25.* R12(i)^2)/(2.* (2.25^2 - R12(i)^2)));
% end




allstep=[];
allstep=[allstep; compstep; rpi1step; rpi2step];
allstep=sortrows(allstep);


figure(4)
data=[0 0 1
    0 2.25 1
    -5.13 0 1];
title('RSSI Localization');
h1=viscircles([0 0],0);
h2=h1;
h3=h1;

mapshow ([-20 20],[-20 20],'DisplayType','point')
grid 'on'
hold 'on'
text(data(1,1),data(1,2),'Co');
text(data(2,1),data(2,2),'RPi1');
text(data(3,1),data(3,2),'RPi2');
geoshow(data(1,2), data(1,1),...
    'DisplayType', 'point',...
    'Marker', 'o',...
    'MarkerEdgeColor', 'g',...
    'MarkerFaceColor', 'g',...
    'MarkerSize', (data(1,3)))
geoshow(data(2,2), data(2,1),...
    'DisplayType', 'point',...
    'Marker', 'o',...
    'MarkerEdgeColor', 'b',...
    'MarkerFaceColor', 'b',...
    'MarkerSize', (data(2,3)))
geoshow(data(3,2), data(3,1),...
    'DisplayType', 'point',...
    'Marker', 'o',...
    'MarkerEdgeColor', 'r',...
    'MarkerFaceColor', 'r',...
    'MarkerSize', (data(3,3)))
for i=1:1:length(allstep)
    compfind=find(compstep==allstep(i));
    rpi1find=find(rpi1step==allstep(i));
    rpi2find=find(rpi2step==allstep(i));
    compr=1;
    rpi1r=1;
    rpi2r=1;
    if ~isempty(compfind)
        compr=compd(compfind);
        set(h1,'Visible', 'off');
    end
    if ~isempty(rpi1find)
        rpi1r=rpi1d(rpi1find);
        set(h2,'Visible', 'off');
    end
    if ~isempty(rpi2find)
        rpi2r=rpi2d(rpi2find);
        set(h3,'Visible', 'off');
    end
    
    data=[0 0 compr
        0 2.25 rpi1r
        -5.13 0 rpi2r];

    if ~isempty(compfind)
        h1=viscircles([data(1,1) data(1,2)],data(1,3),'Color','g');
        set(h1,'Visible', 'on');
    end
    if ~isempty(rpi1find)
        h2=viscircles([data(2,1) data(2,2)],data(2,3),'Color','b');
        set(h2,'Visible', 'on');
    end
    if ~isempty(rpi2find)
        h3=viscircles([data(3,1) data(3,2)],data(3,3),'Color','r');
        set(h3,'Visible', 'on');
    end
%     drawnow;
%     pause(0.01);
end

figure
h=ezplot('sqrt(x^2+(y-2.25)^2)-sqrt((x-(-5.13))^2+y^2)=1.82',[-6,0,-20,5])
hold on
% plot(-8:1:1,2.25+-3.49)
% fplot(sqrt(y^2)-sqrt((y-2.25)^2)=-3.49)

% plot(h.XData,h.YData,h.ZData);
LIM = get(gca,{'xlim','ylim'});
L = line((1.666+(-5.13))/2*[1 1],LIM{2});
L2=line(LIM{1},(-3.49+2.25)/2*[1 1]);