1. put interfaces into monitor mode 
    (assuming the interface is wlan1, may vary depend on your device. check with iw list to see if monitor mode is supported)
    sudo airmon-ng start wlan1

    or

    sudo ifconfig wlan1 down
    sudo iwconfig wlan1 mode monitor
    sudo ifconfig wlan1 up
2. check if the interface is in monitor
    iwconfig
    under "Mode", should show Auto instead of Managed

    or 

    sudo tcpdump -i wlan1
    should show frequency and signal strength if successfully in monitor mode

3. capture with tcpdump and save as .pcapng
    sudo tcpdump -i wlan1 --time-stamp-precision nano -e -v -w /path/to/file/filename.pcapng -Z root -U

4. if using TL-WN822N or TL-WN722N with REALTEK chipset, can just run my scrpit "just_tcp.sh", which will auto detect and set monitor mode and capture
    (filename without .pcapng)
    sudo ./just_tcp.sh -w /path/to/file/filename

5. matchfull.m plots the pcap data
