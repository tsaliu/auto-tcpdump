#!/bin/bash
set -e

echo 'Getting valid dev ...'
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -i|--interface)
    INTERFACE="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
if [[ -n $1 ]]; then
    echo "Invalid tcp.sh argument format"
    tail -1 "$1"
fi
echo "Capturing on interface: ${INTERFACE}"
#sudo tcpdump --linktype=IEEE802_11 --time-stamp-precision nano -e -i ${INTERFACE} -v type mgt subtype probe-resp or subtype prob-req -I
#-y IEEE802_11_RADIO
#sudo tcpdump --time-stamp-precision nano -e -i ${INTERFACE} -v -y IEEE802_11_PRISM
#sudo tcpdump -i ${INTERFACE}  ether host b8:27:eb:1c:b8:9d --time-stamp-precision nano -e
#sudo tcpdump -i ${INTERFACE} ether host e0:5f:45:ae:e1:8b --time-stamp-precision nano -e -v
sudo tcpdump -i ${INTERFACE} --time-stamp-precision nano -e -v



