#!/bin/bash
set -e

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -w|--writefile)
    WRITE="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters
if [[ -n $1 ]]; then
    echo "Invalid tcp.sh argument format"
    tail -1 "$1"
fi

writecmd="-w $WRITE.pcapng -Z root"
echo "Writting at ${writecmd}"


string="Auto"
string2="REALTEK"
if all_int=`(iwconfig | grep $string)`; then
	echo "Monitor Mode Already Enabled."
	
	set -- $all_int
	
	echo $1
	
	if find2=`(iwconfig | grep $string2)`;then
		set -- $find2
		echo "Start Capturing at $1"
		sudo tcpdump -i ${1} --time-stamp-precision nano -e -v $writecmd -U
	else 
		echo "Error: Something is wrong."
	fi
else
	echo "Monitor Mode Not Enabled"
	echo "Looking for TL-WN822N Interface to Enable Monitor Mode .."
	if find2=`(iwconfig | grep $string2)`;then
		set -- $find2
		echo "Found Interface at: $1" 
		
		echo "Setting Monitor Mode"
		sudo ifconfig ${1} down
		sleep 5
		sudo iwconfig ${1} mode monitor
		sudo ifconfig ${1} up
		sleep 5
		echo "Start Capturing at $1"
		sudo tcpdump -i ${1} --time-stamp-precision nano -e -v $writecmd -U
	else
		echo "Error: Cannot find interface"
	fi
fi

#echo $all_int


